/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.chrono.Chronology;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Callback;
import sugar.rush.model.Customer;
import sugar.rush.model.CustomerDAO;

/**
 * FXML Controller class
 *
 * @author Marc Gil Sadumiano
 */
public class CustomersController implements Initializable, MyController, EventHandler<MouseEvent> {

    
    @FXML
    private Button btnBack, btnSave, btnDelete, btnNewCustomer, btnCreate, btnCancel;
    
    @FXML
    private Label min1, close1;
    
    @FXML
    private TextField txtFirstName, txtLastName, txtTown;
    
    @FXML
    private TableView<Customer> tblCustomers;
    
    @FXML
    private DatePicker dateDob;
    
    @FXML
    private ComboBox comboGender;
    
    @FXML
    private TableColumn<Customer, String> colFirstName, colLastName, colGender, colTown;
    @FXML
    private TableColumn<Customer, LocalDate> colDOB;
    
    private Stage stage = null;
    private Scene sceneMenu = null;
    private CustomerDAO cdao;
    private Customer selectedCustomer;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        System.out.println("Customers loaded...");
        try {
            cdao = new CustomerDAO();
        } catch (SQLException ex) {
            Logger.getLogger(CustomersController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        createCustomerMode(false);
        
        //add event handlers to nodes
        btnBack.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        close1.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        min1.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        btnCreate.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        btnCancel.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        btnSave.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        btnDelete.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        btnNewCustomer.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        tblCustomers.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        
        //combo box data
        comboGender.setItems(FXCollections.observableArrayList(Customer.GENDER.values()));
        comboGender.getSelectionModel().select(Customer.GENDER.Male);
        
        //tableview column data
        colFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        colLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        colGender.setCellValueFactory(new PropertyValueFactory<>("gender"));
        colDOB.setCellValueFactory(new PropertyValueFactory<>("dob"));
        colTown.setCellValueFactory(new PropertyValueFactory<>("town"));
        
        try{
            ObservableList<Customer> data = FXCollections.observableArrayList(cdao.getAll());
            tblCustomers.setItems(data);
        }
        catch(SQLException x){
            
        }    
    }
    
    @Override
    public void setStageAndMainMenu(Stage stage, Scene scene) {
        this.sceneMenu = scene;
        this.stage = stage;
        System.out.println("customer page opened!");
        stage.setOnCloseRequest(event -> {
            System.out.println("close request setup");
        });
        
    }

    @Override
    public void handle(MouseEvent event) {
        
        Object obj = event.getSource();
        
        if(obj == btnBack) {
            stage.setScene(sceneMenu);
        }
        
        else if(obj == close1) {
            stage.close();
        }
        
        else if(obj == min1) {
            stage.setIconified(true);
            
        }
        
        else if(obj == tblCustomers){
            selectedCustomer = tblCustomers.getSelectionModel().getSelectedItem();
            
            if(selectedCustomer != null){
                txtFirstName.setText(selectedCustomer.getFirstName());
                txtLastName.setText(selectedCustomer.getLastName());
                comboGender.getSelectionModel().select(Customer.GENDER.valueOf(selectedCustomer.getGender()));
                dateDob.getEditor().setText(selectedCustomer.getDob().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))); 
                dateDob.setValue(selectedCustomer.getDob());
                txtTown.setText(selectedCustomer.getTown());
            }
        }
        
        else if(obj == btnCreate){
            
            //TEMPORARY CODE!!!
            if(addCustomer()){
                
                tblCustomers.refresh();
                clearInputs();
                createCustomerMode(false);
                System.out.println("customer added!");
            }
            
        }
        
        else if(obj == btnCancel){
            createCustomerMode(false);
            clearInputs();
        }
        
        else if(obj == btnSave){
            
            //confirmation dialog
            if(saveCustomer()){
                tblCustomers.refresh();
                System.out.println("Customer saved");
                //alert
            }else{
                //alert
                System.out.println("Customer not saved");
            }            
        }
        
        else if(obj == btnDelete){
        
            //confirmation dialog
            if(deleteCustomer()){
                //alert
                System.out.println("Customer deleted");
                tblCustomers.getItems().remove(selectedCustomer);
                tblCustomers.getSelectionModel().clearSelection();
            }else{
                //alert
                System.out.println("Customer not deleted");
            }
            
        }
        
        else if(obj == btnNewCustomer){
            
            createCustomerMode(true);
            clearInputs();
        }
        
    }
    
    private void createCustomerMode(boolean b){
    
        if(b){
            btnCancel.setVisible(true);
            btnCreate.setVisible(true);
            tblCustomers.setDisable(true);
            btnDelete.setVisible(false);
            btnSave.setVisible(false);
            btnNewCustomer.setVisible(false);
            txtFirstName.requestFocus();
        }
        else{
            
            btnCancel.setVisible(false);
            btnCreate.setVisible(false);
            btnDelete.setVisible(true);
            btnSave.setVisible(true);
            btnNewCustomer.setVisible(true);
            tblCustomers.setDisable(false);
            
        }
        
    }
    
    private void clearInputs(){
        txtFirstName.clear();
        txtLastName.clear();
        comboGender.getSelectionModel().select(Customer.GENDER.Male);
        dateDob.setValue(null); //fixed bug for selecting same date and doesn't set text when inputs are cleared
        dateDob.getEditor().clear();
        
        txtTown.clear();
    }
    
    private Boolean addCustomer(){
        
        //TEMPORARY CODE!!!
        Customer c = new Customer();
        
        c.setFirstName(txtFirstName.getText());
        c.setLastName(txtLastName.getText());
        c.setDob(dateDob.getValue());
        c.setGender(comboGender.getValue().toString());
        c.setTown(txtTown.getText());
        System.out.println(dateDob.getValue());
        try {
            
            if(cdao.add(c)){
                return tblCustomers.getItems().add(c);
            }
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(CustomersController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
        
    }
    
    private Boolean saveCustomer(){
        
        if(selectedCustomer != null){
            
            selectedCustomer.setFirstName(txtFirstName.getText());
            selectedCustomer.setLastName(txtLastName.getText());
            selectedCustomer.setDob(dateDob.getValue());
            selectedCustomer.setGender(comboGender.getValue().toString());
            selectedCustomer.setTown(txtTown.getText());
            
            try {
                return (cdao.update(selectedCustomer));
                
            } catch (SQLException ex) {
                Logger.getLogger(CustomersController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        return false;
        
    }
    
    private Boolean deleteCustomer(){
        
        
        if(selectedCustomer != null){
            
            try {
                return (cdao.delete(selectedCustomer));
            } catch (SQLException ex) {
                Logger.getLogger(CustomersController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    
}
