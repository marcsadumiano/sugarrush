/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import sugar.rush.model.Customer;
import sugar.rush.model.CustomerDAO;
import sugar.rush.model.Order;
import sugar.rush.model.OrderItem;
import sugar.rush.model.OrderDAO;
import sugar.rush.model.OrderItemDAO;

/**
 * FXML Controller class
 *
 * @author Marc Gil Sadumiano
 */
public class OrdersController implements Initializable, MyController, EventHandler<MouseEvent> {

    @FXML
    private Button btnBack, btnNewOrder, btnDelete, btnCancel, btnCreate, btnSave, btnAddItem;
    
    @FXML
    private Label close1, min1, lblOrderNum;
    
    @FXML
    private ComboBox<Customer> comboCustomer;
    
    @FXML private DatePicker dateOrdered;
    
    @FXML private TableView<Order> tblOrders;
    @FXML private TableView<OrderItem> tblOrderItems;
    
    //orders tableview
    @FXML private TableColumn<Order, Integer> colId;
    @FXML private TableColumn<Order, Customer> colCustomer;
    @FXML private TableColumn<Order, Double> colTotal;
    @FXML private TableColumn<Order, LocalDate> colDate;
    
    //order items tableview
    @FXML private TableColumn<OrderItem, String> colName, colCategory;
    @FXML private TableColumn<OrderItem, Double> colPrice;
    
    private Scene sceneMenu, scene;
    private Stage stage;
    private Order selectedOrder;
    private OrderItem selectedOrderItem;
    private TableView selectedTable;
    private CustomerDAO cdao;
    private OrderDAO odao;
    private OrderItemDAO oidao;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        try {
            cdao = new CustomerDAO();
            odao = new OrderDAO();
            oidao = new OrderItemDAO();
            ObservableList<Order> data = FXCollections.observableArrayList(odao.getAll());
            
            tblOrders.setItems(data);
            comboCustomer.setItems(FXCollections.observableArrayList(cdao.getAll()));
        } catch (SQLException ex) {
            Logger.getLogger(OrdersController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        createOrderMode(false);
        btnAddItem.setVisible(false);
        
        //add event handlers to nodes
        btnBack.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        close1.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        min1.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        tblOrders.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        tblOrderItems.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        btnNewOrder.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        btnCreate.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        btnCancel.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        btnSave.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        btnDelete.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        btnAddItem.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        
        //tblOrders column data
        colId.setCellValueFactory(new PropertyValueFactory<>("orderID"));
        colCustomer.setCellValueFactory(new PropertyValueFactory<>("customer"));
        colDate.setCellValueFactory(new PropertyValueFactory<>("date"));
        colTotal.setCellValueFactory(new PropertyValueFactory<>("total"));
        
        //tblOrderItems column data
        colName.setCellValueFactory((col) -> new SimpleStringProperty(col.getValue().getRecipe().getName()));
        colCategory.setCellValueFactory((col) -> new SimpleStringProperty(col.getValue().getRecipe().getCategory()));
        colPrice.setCellValueFactory((col) -> new SimpleDoubleProperty(col.getValue().getRecipe().getPrice()).asObject());
        
   
    }    

    @Override
    public void setStageAndMainMenu(Stage stage, Scene scene) {
        this.stage = stage;
        this.sceneMenu = scene;
        
    }

    @Override
    public void handle(MouseEvent event) {
        
        Object obj = event.getSource();
        
        if(obj == btnBack) {
            stage.setScene(sceneMenu);
        }
        
        else if(obj == close1) {
            stage.close();
        }
        
        else if(obj == min1) {
            stage.setIconified(true);
            
        }
        
        else if(obj == tblOrders){
            
            selectedTable = tblOrders;
            selectedOrder = tblOrders.getSelectionModel().getSelectedItem();
            
            if(selectedOrder != null){
                btnAddItem.setVisible(true);
                lblOrderNum.setText(String.valueOf(selectedOrder.getOrderID()));
                comboCustomer.getSelectionModel().select(selectedOrder.getCustomer());
                dateOrdered.setValue(selectedOrder.getDate().toLocalDate());
                dateOrdered.getEditor().setText(selectedOrder.getDate().toLocalDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                try {
                    oidao.getOrderItems(selectedOrder);
                    ObservableList<OrderItem> data = FXCollections.observableArrayList(selectedOrder.getOrderItems());
                    tblOrderItems.setItems(data);
                    tblOrderItems.getItems().addListener(new ListChangeListener<OrderItem>(){
                        
                        @Override
                        public void onChanged(ListChangeListener.Change<? extends OrderItem> c) {
                            if(c.next()){
                                if(c.wasAdded() || c.wasRemoved()){
                                    calculateSelectedOrderTotal();
                                }
                            }
                        }
                        
                    });
                } catch (SQLException ex) {
                    Logger.getLogger(OrdersController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        else if(obj == tblOrderItems){
            
            selectedTable = tblOrderItems;
            selectedOrderItem = tblOrderItems.getSelectionModel().getSelectedItem();
        }
        
        else if(obj == btnNewOrder){
            
            createOrderMode(true);
            clearInputs();
            btnAddItem.setVisible(true);
        }
        
        else if(obj == btnSave){
            
            if(saveOrder()){
                tblOrders.refresh();
                System.out.println("Order saved");
                //alert
                if(addOrderItems()){
                    System.out.println("Order items saved");
                }else{
                    System.out.println("Order items not saved");
                }
            }else{
                System.out.println("Order not saved");
                //alert
            }
            
        }
        
        else if(obj == btnDelete){
            if(selectedTable == tblOrders){
                if(deleteOrder()){
                    tblOrders.getItems().remove(selectedOrder);
                    tblOrders.getSelectionModel().clearSelection();
                    clearInputs();
                    System.out.println("Order deleted");
                }else{
                    System.out.println("Order not deleted");
                }
            }else if(selectedTable == tblOrderItems){
                if((selectedOrderItem.getOrderItemID() != 0) && deleteOrderItem()){
                    tblOrderItems.getItems().remove(selectedOrderItem);
                    tblOrderItems.getSelectionModel().clearSelection();
                    System.out.println("Order item deleted");
                }else if(selectedOrderItem.getOrderItemID() == 0){
                    tblOrderItems.getItems().remove(selectedOrderItem);
                    tblOrderItems.getSelectionModel().clearSelection();
                    System.out.println("Order item w/o id deleted");
                }else{
                    System.out.println("Order item not deleted");
                }
            }
        }
        
        else if(obj == btnCreate){
            if(addOrder()){
                tblOrders.refresh();
                createOrderMode(false);
                tblOrders.getSelectionModel().select(selectedOrder);
                selectedTable = tblOrders;
                System.out.println("order created");
                //saveOrderItems
                if(addOrderItems()){
                    
                    if(!tblOrderItems.getItems().isEmpty()){
                        calculateSelectedOrderTotal();
                    }
                    System.out.println("Order and order items created");
                }else{
                    System.out.println("Order and order items not created");
                }
            }
        }
        
        else if(obj == btnCancel){
            System.out.println("cancel button");
            createOrderMode(false);
            clearInputs();
            
        }
        
        else if(obj == btnAddItem){
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("view/AddOrderItem.fxml"));
                Parent root = (Parent)loader.load();
                Scene scene = new Scene(root);
                Stage stage2 = new Stage(StageStyle.UTILITY);
                stage2.setTitle("Add item PAGE");
                stage2.setScene(scene);
                stage2.setTitle("SugarRush");
                stage2.initOwner(stage);
                stage2.initModality(Modality.APPLICATION_MODAL);
                AddOrderItemController controller = (AddOrderItemController)loader.getController();
                controller.setStageAndMainMenu(stage2, scene);
                if(selectedOrder != null){
                    controller.setOrderID(selectedOrder.getOrderID());
                }else{
                    controller.setOrderID(0);
                }
                
                stage2.showAndWait();
            } catch (IOException ex) {
                Logger.getLogger(OrdersController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
    private Boolean addOrder(){
        
        Order order = new Order();
        order.setCustomer(comboCustomer.getSelectionModel().getSelectedItem());
        order.setDate(dateOrdered.getValue().atTime(LocalTime.now()));
        
        try {
            if(odao.add(order)){
                selectedOrder = order;
                return tblOrders.getItems().add(order);
            }
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(OrdersController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
    private Boolean saveOrder(){
        
        if(selectedOrder != null){
            
            LocalTime time = selectedOrder.getDate().toLocalTime();
            selectedOrder.setDate(dateOrdered.getValue().atTime(time));
            Customer customer = comboCustomer.getSelectionModel().getSelectedItem();
            selectedOrder.setCustomer(customer);
            
            try {
                return (odao.update(selectedOrder));
            } catch (SQLException ex) {
                Logger.getLogger(OrdersController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        return false;
    }
    
    private Boolean deleteOrder(){
        
        
        if(selectedOrder != null){

            try {
                return (odao.delete(selectedOrder));
            } catch (SQLException ex) {
                Logger.getLogger(OrdersController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
    
    private Boolean deleteOrderItem(){
        
        
        if(selectedOrderItem != null){

            try {
                return (oidao.delete(selectedOrderItem));
            } catch (SQLException ex) {
                Logger.getLogger(OrdersController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
    
    private Boolean deleteOrderItems(){
        
        if(selectedOrder != null){
            for(OrderItem item : tblOrderItems.getItems()){
                
                try {
                    if(!oidao.delete(item)){
                        return false;
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(OrdersController.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
            return true;
        }
        
        return false;
    }
    
    
    private Boolean addOrderItems(){
        
        if(selectedOrder != null){
            
            for(OrderItem item : tblOrderItems.getItems()){
                //only save order items with no id(this means it hasnt been added to the databse before)
                if(item.getOrderItemID() == 0){
                    
                    //set non 0 orderID
                    if(item.getOrderID() == 0){
                        item.setOrderID(selectedOrder.getOrderID());
                    }
                    
                    try {
                        if(!oidao.add(item)){
                            return false;
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(OrdersController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            return true;
        }
        
        return false;
    }
    
    private void calculateSelectedOrderTotal(){
        
        if(selectedOrder != null){
            System.out.println("calculateSelectedOrderTotal() called");
            ObservableList<OrderItem> items = tblOrderItems.getItems();
            double newTotal = 0;
            for(OrderItem item : items){
                newTotal += item.getRecipe().getPrice();
            }
            selectedOrder.setTotal(newTotal);
            tblOrders.refresh();
        }
        
    }
    
    private void clearInputs(){
        
        selectedOrder = null;
        
        lblOrderNum.setText("");
        comboCustomer.getSelectionModel().select(null);
        dateOrdered.setValue(null);
        dateOrdered.getEditor().setText("");
        tblOrderItems.getItems().clear();
        btnAddItem.setVisible(false);
    }
    
    private void createOrderMode(boolean b){
        
        if(b){
            
            selectedOrder = null;
            
            tblOrders.getSelectionModel().clearSelection();
            selectedTable = null;
            btnDelete.setVisible(false);
            btnSave.setVisible(false);
            btnCreate.setVisible(true);
            btnCancel.setVisible(true);
            btnNewOrder.setVisible(false);
            tblOrders.setDisable(true);
            comboCustomer.requestFocus();
        }else{
            btnDelete.setVisible(true);
            btnSave.setVisible(true);
            btnCreate.setVisible(false);
            btnCancel.setVisible(false);
            btnNewOrder.setVisible(true);
            tblOrders.setDisable(false);
        }
        
    }
    
}
