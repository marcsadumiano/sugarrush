/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush;

import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Marc Gil Sadumiano
 */
public interface MyController {
    
    public void setStageAndMainMenu(Stage stage, Scene scene);
    
}
