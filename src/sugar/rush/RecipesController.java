/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush;

import com.sun.media.jfxmedia.logging.Logger;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import sugar.rush.model.RecipeDAO;


/**
 * FXML Controller class
 *
 * @author mgsadumiano
 */
public class RecipesController implements Initializable, MyController, EventHandler<MouseEvent> {

    private Stage stage;
    private Scene sceneMainMenu;
    @FXML
    private Label close1;
    @FXML
    private Label min1;
    @FXML
    private FlowPane fpRecipeCategories;
    @FXML
    private Button btnBack;
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        System.out.println("initialize section");
        
        close1.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        min1.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        btnBack.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        
        
    }    
    
    private void setUpRecipeCategories(){
        
        Scene recipeScene = stage.getScene();
        //FlowPane fpRecipeCategories = (FlowPane) recipeScene.lookup("#fpRecipeCategories");
        //fpRecipeCategories.setStyle("-fx-background-color: blue;");
        fpRecipeCategories.orientationProperty().setValue(Orientation.VERTICAL);
        fpRecipeCategories.setHgap(10);
        fpRecipeCategories.setVgap(10);
        
        //get categories from db
        try{
            RecipeDAO recipeDb = new RecipeDAO();
            String[] categories = recipeDb.getCategories();
            
            for(String category : categories){
                VBox pane = new VBox();
                pane.setAlignment(Pos.CENTER);
                pane.getStyleClass().add("pane");
                pane.setPrefHeight(110);
                pane.setPrefWidth(150);
                pane.setSpacing(5);
                
                pane.getChildren().add(new Label(category));
                
                fpRecipeCategories.getChildren().add(pane);
            }

            
        }
        catch(SQLException ex){
            Logger.logMsg(Logger.ERROR, "Error accessing Recipes table");
        }
        
        
    }

    @Override
    public void setStageAndMainMenu(Stage stage, Scene scene) {
        System.out.println("setStageAndMainMenu section");
        this.stage = stage;
        this.sceneMainMenu = scene;
        
        setUpRecipeCategories();
    }

    @Override
    public void handle(MouseEvent event) {
        
        Object obj = event.getSource();
        
        if(obj == close1){
            System.out.println("close1");
            this.stage.close();
        }
        
        else if(obj == min1){
            this.stage.setIconified(true);
        }
        
        else if(obj == btnBack){
            System.out.println("btnBack");
            this.stage.setScene(sceneMainMenu);
        }
        
    }
    
}
