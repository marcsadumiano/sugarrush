/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush.view;

import java.io.IOException;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Marc Gil Sadumiano
 */
public class DisplayTemplate {
    
    
    public static void display() {
        Stage stage = new Stage();
        VBox vbox = new VBox();
        vbox.getChildren().addAll(new Label("Marc"), new Label("Sadumiano"), new Label("April 26, 1996"), new Label("Laoag City"));
        
        
        Scene scene = new Scene(vbox, 500, 500);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
        
    }
    
}
