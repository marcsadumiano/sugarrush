/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import sugar.rush.model.OrderItem;
import sugar.rush.model.Recipe;
import sugar.rush.model.RecipeDAO;

/**
 * FXML Controller class
 *
 * @author mgsadumiano
 */
public class AddOrderItemController implements Initializable, MyController, EventHandler<MouseEvent> {

    
    @FXML
    private ComboBox<String> comboCategory;
    
    @FXML 
    private ComboBox<Recipe> comboProduct;
    
    @FXML
    private Label lblPrice; 
    
    @FXML
    private Button btnCancel, btnSave;
    
    private OrdersController ordersController;
    private Stage stage;
    private Scene scene;
    private RecipeDAO rdao;
    private ObservableList<Recipe> recipesModel;
    private int orderID;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        
        try{
            rdao = new RecipeDAO();
            comboCategory.setItems(FXCollections.observableArrayList(rdao.getCategories()));
            recipesModel = FXCollections.observableArrayList();
            comboProduct.setItems(recipesModel);
            
            comboCategory.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
            comboProduct.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
            
        }catch(SQLException ex){
            Logger.getLogger(AddOrderItemController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //setup node model items listener.
        itemListeners();
        
        //add listeners
        btnCancel.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        btnSave.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        
        
        
    }  
    
    private void itemListeners(){
        
        comboCategory.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>(){
            
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if( newValue != null && !(newValue.isEmpty()) ){
                    comboProduct.getSelectionModel().clearSelection();
                    lblPrice.setText("");
                    try {
                        ObservableList<Recipe> data = FXCollections.observableArrayList(rdao.findAll(recipe -> recipe.getCategory().equals(newValue)));
                        comboProduct.setItems(data);
                    } catch (SQLException ex) {
                        Logger.getLogger(AddOrderItemController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            
        });
        
        comboProduct.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Recipe>(){
            
            @Override
            public void changed(ObservableValue<? extends Recipe> observable, Recipe oldValue, Recipe newValue) {
                if(newValue != null){
                    lblPrice.setText(String.valueOf(newValue.getPrice()));
                }
            }
            
        });
    }

    @Override
    public void handle(MouseEvent event) {
        
        Object obj = event.getSource();
        
        if(obj == btnCancel){
           stage.close();
        }
        
        else if(obj == btnSave){
            
            Stage parentStage = (Stage)stage.getOwner();
            //System.out.println(parentStage.getTitle());
            System.out.println(comboProduct.getSelectionModel().getSelectedItem().toString());
            TableView tblOrderItems = (TableView)parentStage.getScene().lookup("#tblOrderItems");
            tblOrderItems.getItems().add(new OrderItem(orderID, comboProduct.getSelectionModel().getSelectedItem()));
            stage.close();
        }
        
    }
    
    public void setOrderID(int orderID){
        this.orderID = orderID;
    }

    @Override
    public void setStageAndMainMenu(Stage stage, Scene scene) {
        this.scene = scene;
        this.stage = stage;
    }
    
}
