/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush;

import java.sql.Connection;
import sugar.rush.model.*;

import java.awt.Point;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.ImageCursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import resources.NodeMoveable;
import sugar.rush.db.DbConnection;

/**
 *
 * @author Marc Gil Sadumiano
 */
public class SugarRush extends Application {
    
    /*
    ClassName.class.getResource('path') or getClass().getResource()
        1)when there isn't a '/' at the start of path, loader looks from package folder of the class instance used
        2)when there is, loader looks from root folder/src folder
    */
    
    @Override
    public void start(Stage stage) throws Exception {
        
        //Parent root = FXMLLoader.load(getClass().getResource("MainMenu.fxml"));
        FXMLLoader loader = new FXMLLoader(getClass().getResource("view/MainMenu.fxml"));
        Parent root = (Parent) loader.load();
        MainMenuController controller = (MainMenuController) loader.getController();
        String css = getClass().getResource("/resources/Style.css").toExternalForm();
        Scene scene = new Scene(root);
        controller.setStageAndMainMenu(stage, scene);
        scene.getStylesheets().add(css);
        //System.out.println(getClass().getResource("view/MainMenu.fxml").toExternalForm());
        
        //set cursor
//        Image img = new Image("/resources/heart0.png");
//        ImageCursor c = new ImageCursor(img, 5, 1);
//        scene.setCursor(c);
        
        
        //moveable stage
        NodeMoveable.Apply(stage, scene, "#dragger");
        
        stage.setScene(scene);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setResizable(false);
        stage.setTitle("Sugar Rush");
        //stage.getIcons().add(new Image("/resources/Statistics.png"));
        stage.show();
    }
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
   
    
}
