/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import javafx.collections.FXCollections;

/**
 *
 * @author mgsadumiano
 */
public class OrderItemDAO extends SugarRushDAO<OrderItem>{
    
    private RecipeDAO recDAO;
    
    public OrderItemDAO() throws SQLException {
        recDAO = new RecipeDAO();
    }

    @Override
    public List<OrderItem> getAll() throws SQLException {
        
        List<OrderItem> items = new ArrayList<>();
        
        ResultSet rs = connection.createStatement().executeQuery("SELECT id, orderID, recipeID FROM OrderItem");
        while(rs.next()){
            
            int orderItemID = rs.getInt("id");
            int orderID = rs.getInt("orderID");
            int recipeID = rs.getInt("recipeID");
            
            Recipe recipe = recDAO.find((p) -> p.getRecipeID() == recipeID);
            
            OrderItem oi = new OrderItem(orderItemID, orderID, recipe);
            items.add(oi);
            
        }
        
        return items;
    }
    
    public void getOrderItems(Order order) throws SQLException {
        List<OrderItem> items = new ArrayList<>();
        String sql = "SELECT id, recipeID FROM OrderItem WHERE orderID = " + order.getOrderID();
        
        ResultSet rs = connection.createStatement().executeQuery(sql);
        
        while(rs.next()){
            
            int orderItemID = rs.getInt("id");
            int orderID= order.getOrderID();
            int recipeID = rs.getInt("recipeID");
            
            Recipe recipe = recDAO.find((p) -> p.getRecipeID() == recipeID);
            
            OrderItem oi = new OrderItem(orderItemID, orderID, recipe);
            items.add(oi);
        }
        order.setOrderItems(items.toArray(new OrderItem[items.size()]));
        
    }

    @Override
    public OrderItem find(Predicate<OrderItem> p) throws SQLException {
        
        for(OrderItem item : getAll()){
            if(p.test(item)){
                return item;
            }
        }
        return null;
        
    }

    @Override
    public Boolean add(OrderItem t) throws SQLException {
        String sql = "INSERT INTO `OrderItem`(orderID, recipeID) "
                + "VALUES(?,?)";
        PreparedStatement ps = connection.prepareStatement(sql);
        
        ps.setInt(1, t.getOrderID());
        ps.setInt(2, t.getRecipe().getRecipeID());
        
        if(ps.executeUpdate() == 1){
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            System.out.println("orderitem insert id " + rs.getInt(1));
            t.setOrderItemID(rs.getInt(1));
            return true;
        }else{
            return false;
        }
    }

    @Override
    public Boolean update(OrderItem t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean delete(OrderItem t) throws SQLException {
        
        OrderItem item = find(oi -> oi.getOrderItemID() == t.getOrderItemID());
        
        if(item != null){
            
            String sql = "DELETE FROM OrderItem WHERE id = " + item.getOrderItemID();
            int count = connection.createStatement().executeUpdate(sql);
            System.out.println("order item/s deleted: " + count);
            return (count > 0);
            
        }
        return false;
    }

    
    
}
