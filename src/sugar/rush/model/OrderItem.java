/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush.model;

/**
 *
 * @author mgsadumiano
 */
public class OrderItem {
    
    private int orderItemID;

    
    private int orderID;
    private Recipe recipe;
    
    public OrderItem(int orderID, Recipe recipe){
        this.recipe = recipe;
    }
    
    public OrderItem(int id, int orderID, Recipe recipe){
        this.orderItemID = id;
        this.orderID = orderID;
        this.recipe = recipe;
    }

    public int getOrderItemID() {
        return orderItemID;
    }

    public void setOrderItemID(int orderItemID) {
        this.orderItemID = orderItemID;
    }
    
    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }
    
    
    
}
