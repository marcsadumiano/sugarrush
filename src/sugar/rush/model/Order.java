/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush.model;

import java.time.LocalDateTime;

/**
 *
 * @author mgsadumiano
 */
public class Order {
    
    private int orderID;
    private Customer customer;
    private LocalDateTime date;
    private double total;
    private OrderItem[] orderItems;
    
    public Order(){}
    
    public Order(int id, Customer customer, LocalDateTime date, double total, OrderItem[] orderItems){
        this.orderID = id;
        this.customer = customer;
        this.date = date;
        this.total = total;
        this.orderItems = orderItems;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public OrderItem[] getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(OrderItem[] orderItems) {
        this.orderItems = orderItems;
    }
    
    
}
