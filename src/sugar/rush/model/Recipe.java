/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush.model;

/**
 *
 * @author mgsadumiano
 */
public class Recipe {
    
    private int recipeID;
    private String name;
    private String description;
    private String category;
    private double price;
    private Ingredient[] ingredients;
    private Method[] methods;
    
    
    
    public Recipe(int id, String name, String description, String category, double price, Ingredient[] ingredients, Method[] methods){
        
        this.recipeID = id;
        this.name = name;
        this.description = description;
        this.category = category;
        this.price = price;
        this.ingredients = ingredients;
        this.methods = methods;
        
    }

    public int getRecipeID() {
        return recipeID;
    }

    public void setRecipeID(int recipeID) {
        this.recipeID = recipeID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Ingredient[] getIngredients() {
        return ingredients;
    }

    public void setIngredients(Ingredient[] ingredients) {
        this.ingredients = ingredients;
    }

    public Method[] getMethods() {
        return methods;
    }

    public void setMethods(Method[] methods) {
        this.methods = methods;
    }

    @Override
    public String toString() {
        return this.getName();
    }
    
    
    
}
