/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 *
 * @author mgsadumiano
 */
public class RecipeDAO extends SugarRushDAO<Recipe>{
    
    public RecipeDAO() throws SQLException {
        
    }

    @Override
    public List<Recipe> getAll() throws SQLException {
        
        List<Recipe> recipes = new ArrayList<>();
        ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM Recipe");
        
        while(rs.next()){
            int id = rs.getInt("id");
            String name = rs.getString("name");
            String description = rs.getString("description");
            String category = rs.getString("category");
            double price = rs.getDouble("price");
            
            Recipe recipe = new Recipe(id, name, description, category, price, null, null);
            recipes.add(recipe);
            
        }
        
        return recipes;
        
    }

    @Override
    public Recipe find(Predicate<Recipe> p) throws SQLException {
        
        for(Recipe recipe : getAll()){
            if(p.test(recipe)){
                return recipe;
            }
        }
        
        return null;
    }
    
    public List<Recipe> findAll(Predicate<Recipe> p) throws SQLException {
        
        List<Recipe> recipes = new ArrayList<>();
        
        for(Recipe recipe : getAll()){
            if(p.test(recipe)){
                recipes.add(recipe);
            }
        }
        
        return recipes;
        
    }

    @Override
    public Boolean add(Recipe t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean update(Recipe t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Boolean delete(Recipe t) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public String[] getCategories() throws SQLException {
        
        ArrayList<String> categories = new ArrayList<>();
        
        ResultSet rs = connection.createStatement().executeQuery("SELECT DISTINCT category FROM Recipe");
        
        while(rs.next()){
            categories.add(rs.getString("category"));
        }
        
        return categories.toArray(new String[categories.size()]);
        
    }
    
}
