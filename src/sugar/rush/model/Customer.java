/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush.model;

import java.time.LocalDate;

/**
 *
 * @author mgsadumiano
 */
public class Customer {

    public enum GENDER{
        Male,
        Female;
        
        @Override
        public String toString() {
            return super.toString();
        }
    }
    
    private int userID;
    private String firstName;
    private String lastName;
    private String gender;
    private LocalDate dob;
    private String town;
    
    public Customer(){}
    
    public Customer(int id){
        this.userID = id;
    }
    
    public Customer(int id, String f, String l, String gender, LocalDate dob, String town){
        this.userID = id;
        this.firstName = f;
        this.lastName = l;
        this.gender = gender;
        this.dob = dob;
        this.town = town;
    }
    
    public int getUserID() {
        return userID;
    }
    
    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    @Override
    public String toString() {
        return this.firstName  + " " + this.lastName; //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}
