/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush.model;

import sugar.rush.db.DbConnection;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mgsadumiano
 * @param <T> represents one of the Sugar Rush's model classes
 */
public abstract class SugarRushDAO<T> {
    
    protected Connection connection;
    
    public SugarRushDAO() throws SQLException{
        
        connection = DbConnection.getConnection();
        System.out.println("sugarrushdao constructor");
        
    }
    
    public abstract List<T> getAll() throws SQLException;
    public abstract T find(Predicate<T> p) throws SQLException;
    public abstract Boolean add(T t) throws SQLException;
    public abstract Boolean update(T t) throws SQLException;
    public abstract Boolean delete(T t) throws SQLException;
    
}
