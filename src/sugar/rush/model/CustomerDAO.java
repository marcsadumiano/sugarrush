/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 *
 * @author mgsadumiano
 */
public class CustomerDAO extends SugarRushDAO<Customer> {
   
    public CustomerDAO() throws SQLException{
        
    }

    @Override
    public List<Customer> getAll() throws SQLException{
        
        List<Customer> customers = new ArrayList<>();
        ResultSet rs = connection.createStatement().executeQuery("SELECT * FROM Customer ORDER BY firstname");
        while(rs.next()){
            int id = rs.getInt("id");
            String fname = rs.getString("firstname");
            String lname = rs.getString("lastname");
            String gender = rs.getString("gender");
            String dob = rs.getString("dob");
            String town = rs.getString("town");
            customers.add(new Customer(id, fname, lname, gender, LocalDate.parse(dob), town));
        }
        
        return customers;
        
    }

    @Override
    public Customer find(Predicate<Customer> p) throws SQLException {
        
        for(Customer customer : getAll()){
            if(p.test(customer)){
                return customer;
            }
        }
       
        return null;
        
    }

    @Override
    public Boolean add(Customer t) throws SQLException{
        
        PreparedStatement ps = connection.prepareStatement("INSERT INTO Customer(firstname, lastname, gender, dob, town) VALUES(?,?,?,?,?)");
        ps.setString(1, t.getFirstName());
        ps.setString(2, t.getLastName());
        ps.setString(3, t.getGender());
        ps.setString(4, t.getDob().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        ps.setString(5, t.getTown());
        
        if(ps.executeUpdate() == 1){
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            System.out.println("insert id " + rs.getInt(1));
            t.setUserID(rs.getInt(1));
            return true;
        }else
            return false;
        
    }

    @Override
    public Boolean update(Customer t) throws SQLException{
        
        Customer customer = find((c) -> c.getUserID() == t.getUserID());
        
        if(customer != null){
            String sql = "UPDATE Customer " +
                    "SET firstname = ?, " + //1
                    "lastname = ?, " + //2
                    "gender = ?, " + //3
                    "dob = ?, " + //4
                    "town = ? " + //5
                    "WHERE id = ?"; //6
            
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, t.getFirstName());
            ps.setString(2, t.getLastName());
            ps.setString(3, t.getGender());
            ps.setString(4, t.getDob().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            ps.setString(5, t.getTown());
            ps.setInt(6, t.getUserID());
            
            return (ps.executeUpdate() == 1);
        }
        return false;
    }

    @Override
    public Boolean delete(Customer t) throws SQLException{
        
        Customer customer = find((c) -> c.getUserID() == t.getUserID());
        
        if(customer != null){
            String sql = "DELETE FROM Customer WHERE id = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, t.getUserID());
            
            return (ps.executeUpdate() == 1);
        }
        return false;
        
    }

    
    
}
