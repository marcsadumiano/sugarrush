/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 *
 * @author mgsadumiano
 */
public class OrderDAO extends SugarRushDAO<Order>{
    
    private CustomerDAO cdao;
    
    public OrderDAO() throws SQLException {
        super();
        cdao = new CustomerDAO();
    }

    @Override
    public List<Order> getAll() throws SQLException {
        
        List<Order> orders = new ArrayList<>();
        String sql = "SELECT o.id, "
                + "o.date, "
                + "o.total, "
                + "o.customerID "
                + "FROM `Order` as o";
        ResultSet rs = connection.createStatement().executeQuery(sql);
        
        while(rs.next()){
            //order details
            int id = rs.getInt("id");
            LocalDateTime date = LocalDateTime.parse(rs.getString("date"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
            double total = rs.getDouble("total");
            //customer details
            int customerID = rs.getInt("customerID");
            Customer customer = cdao.find((p) -> p.getUserID() == customerID);
            
            Order order = new Order(id, customer, date, total, null);
            orders.add(order);
            
        }
        
        return orders;
        
    }

    @Override
    public Order find(Predicate<Order> p) throws SQLException {
        
        for(Order order : getAll()){
            if(p.test(order)){
                return order;
            }
        }
        return null;
        
    }

    @Override
    public Boolean add(Order t) throws SQLException {
        
        String sql = "INSERT INTO `Order`(date, customerID) "
                + "VALUES(?,?)";
        PreparedStatement ps = connection.prepareStatement(sql);
        
        ps.setString(1, t.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
        ps.setInt(2, t.getCustomer().getUserID());
        
        if(ps.executeUpdate() == 1){
            ResultSet rs = ps.getGeneratedKeys();
            rs.next();
            System.out.println("order insert id " + rs.getInt(1));
            t.setOrderID(rs.getInt(1));
            return true;
        }else{
            return false;
        }
    }

    @Override
    public Boolean update(Order t) throws SQLException {
        
        Order order = find((o) -> o.getOrderID() == t.getOrderID());
        
        if(order != null){
            String sql = "UPDATE `Order` "
                    + "SET date = ?, "
                    + "customerID = ? "
                    + "WHERE id = " + t.getOrderID();
            
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, t.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            ps.setInt(2, t.getCustomer().getUserID());
            
            return (ps.executeUpdate() == 1);
        }
        
        return false;
    }

    @Override
    public Boolean delete(Order t) throws SQLException {
        
        Order order = find((o) -> o.getOrderID() == t.getOrderID());
        
        if(order != null){
            String sql = "DELETE FROM `Order` WHERE id = " + t.getOrderID();
            int count = connection.createStatement().executeUpdate(sql);
            System.out.println("order/s deleted " + count);
            return ( count > 0);
        }
        return false;
        
    }
    
}
