/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author mgsadumiano
 */
public class DbConnection {
    
    private static final String SQCONN = "jdbc:sqlite:dbSugarRush.db"; //creates the .db file if it doesn't find any. (*CREATES AN EMPTY DATABASE*)
    private static final String UNAME = "";
    private static final String PASS = "";
    
    public static Connection getConnection()throws SQLException{
    
        try{
            Class.forName("org.sqlite.JDBC");
            Connection conn = DriverManager.getConnection(SQCONN);
            conn.createStatement().execute("PRAGMA foreign_keys = ON");
            return conn;
        }
        catch(ClassNotFoundException ex){
            ex.printStackTrace();
        }
        
        return null;
    }
    
}
