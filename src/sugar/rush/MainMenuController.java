/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sugar.rush;

import com.sun.deploy.util.FXLoader;
import java.awt.Point;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.ImageCursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import sugar.rush.view.DisplayTemplate;
import resources.NodeMoveable;

/**
 *
 * @author Marc Gil Sadumiano
 */
public class MainMenuController implements Initializable, EventHandler<MouseEvent>, MyController {
    
    
    @FXML
    private ImageView imgCake;
    
    @FXML
    private AnchorPane apOrders, apRecipes, apStatistics, apCustomers;

    @FXML
    private Label close1, min1;
   
    private Stage stage;
    private Scene sceneMenu;
    private String cssResource;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        cssResource = getClass().getResource("/resources/Style.css").toExternalForm();
        
        //event handlers
        apCustomers.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        apOrders.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        apRecipes.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        apStatistics.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        close1.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        min1.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        
    }
    
    @Override
     public void handle(MouseEvent event) {
        
        Object obj =  event.getSource();
        Parent root = null;
        Scene scene = null;
        
        //access current stage's properties
        /*
        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        */
        
        
        if(obj == apCustomers){
            System.out.println("Customer");
            
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("view/Customers.fxml"));
                root = (Parent) loader.load();
                CustomersController custController = (CustomersController)loader.getController();
                custController.setStageAndMainMenu(stage, sceneMenu);
                scene = new Scene(root);
                //setDragger(scene);
                NodeMoveable.Apply(this.stage, scene, "#dragger");
                setCustomCursor(scene);
                scene.getStylesheets().add(cssResource);
                stage.setScene(scene);
                

                
            } catch (IOException ex) {
                Logger.getLogger(MainMenuController.class.getName()).log(Level.SEVERE, "Error loading 'Customers.fxml'.", ex);
            }
            
        }
        
        else if(obj == apOrders) {
            System.out.println("Orders");
            try {
                FXMLLoader loader = new FXMLLoader(SugarRush.class.getResource("view/Orders.fxml"));
                root = (Parent) loader.load();
                OrdersController controller = (OrdersController) loader.getController();
                controller.setStageAndMainMenu(stage, sceneMenu);
                scene = new Scene(root);
                scene.getStylesheets().add(cssResource);
                //setDragger(scene);
                NodeMoveable.Apply(this.stage, scene, "#dragger");
                setCustomCursor(scene);
                stage.setTitle("Orders");
                stage.setScene(scene);
                stage.show();
            } catch (IOException ex) {
                Logger.getLogger(MainMenuController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        else if(obj == apRecipes) {
            System.out.println("Recipes");
            
            try {
                
                FXMLLoader loader = new FXMLLoader(getClass().getResource("view/Recipes.fxml"));
                root = (Parent)loader.load();
                RecipesController controller = (RecipesController)loader.getController();
                controller.setStageAndMainMenu(stage, sceneMenu);
                scene = new Scene(root);
                scene.getStylesheets().add(cssResource);
                //setDragger(scene);
                NodeMoveable.Apply(this.stage, scene, "#dragger");
                setCustomCursor(scene);
                stage.setTitle("Recipes");
                stage.setScene(scene);
                stage.show();
            } catch (IOException ex) {
                Logger.getLogger(MainMenuController.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            
        }
        
        else if(obj == apStatistics) {
            System.out.println("Statistics");
            DisplayTemplate.display();
        }
        
        else if(obj == close1) {
            stage.close();
        }
        
        else if(obj == min1) {
            stage.setIconified(true);
        }
        
    }
     
     private void setCustomCursor(Scene scene) {
         
         //set cursor
//        Image img = new Image("/resources/heart0.png");
//        ImageCursor c = new ImageCursor(img, 5, 1);
//        scene.setCursor(c);
     }
     
     @Override
     public void setStageAndMainMenu(Stage stage, Scene scene){
         this.stage = stage;
         this.sceneMenu = scene;
     }
    
}
