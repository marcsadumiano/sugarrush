/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import java.awt.Point;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 *
 * @author marcg
 */
public class NodeMoveable {
    
    private NodeMoveable(){}
    
    public static void Apply(Stage stage, Scene scene, String nodeId ){
        Point stagePos = new Point();
        Node sp = scene.lookup(nodeId);
        
        sp.setOnMousePressed((MouseEvent event) -> {
            stagePos.x =  (int) event.getSceneX();
            stagePos.y = (int) event.getSceneY();
        });
        
        
        sp.setOnMouseDragged((event) -> {
            stage.setX(event.getScreenX() - stagePos.getX());
            stage.setY(event.getScreenY() - stagePos.getY());
        });
    }
    
}
